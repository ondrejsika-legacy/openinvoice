from django.template.loader import render_to_string

from tex import latex2pdf


def generate(invoice):
    tex_source = render_to_string('generator/invoice/default.tex', {
        'number': invoice.number,
        'variable_symbol': invoice.variable_symbol,
        'issued_on': invoice.issued_on,
        'due_on': invoice.due_on,
        'taxable_fulfillment_due': invoice.taxable_fulfillment_due,
        'note': invoice.note,
        'footer_note': invoice.footer_note,
        'total': invoice.total,
        'line_iterable': invoice.invoiceline_set.all(),
        'currency': invoice.currency,

        'account_name': invoice.account.name,
        'account_street': invoice.account.street,
        'account_street2': invoice.account.street2,
        'account_zip': invoice.account.zip,
        'account_city': invoice.account.city,
        'account_country': invoice.account.country,
        'account_registration_no': invoice.account.registration_no,
        'account_vat_no': invoice.account.vat_no,
        'account_note': invoice.account.note,
        'account_bank_account': invoice.account.bank_account,

        'subject_name': invoice.subject.name,
        'subject_street': invoice.subject.street,
        'subject_street2': invoice.subject.street2,
        'subject_zip': invoice.subject.zip,
        'subject_city': invoice.subject.city,
        'subject_country': invoice.subject.country,
        'subject_registration_no': invoice.subject.registration_no,
        'subject_vat_no': invoice.subject.vat_no,
    })
    return latex2pdf(tex_source)
