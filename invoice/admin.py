# django
from django.contrib import admin

# local
from .models import Invoice, InvoiceLine, InvoicePDF


admin.site.register(Invoice)
admin.site.register(InvoiceLine)
admin.site.register(InvoicePDF)
