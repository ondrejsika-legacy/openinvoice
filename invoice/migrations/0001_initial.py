# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Invoice'
        db.create_table(u'invoice_invoice', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('proforma', self.gf('django.db.models.fields.BooleanField')(default=False)),
            ('number', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('variable_symbol', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('account', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['account.Account'])),
            ('subject', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['subject.Subject'])),
            ('related', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['invoice.Invoice'], null=True, blank=True)),
            ('issued_on', self.gf('django.db.models.fields.DateField')(default=datetime.date.today)),
            ('taxable_fulfillment_due', self.gf('django.db.models.fields.DateField')(default=datetime.date.today)),
            ('due_on', self.gf('django.db.models.fields.DateField')()),
            ('note', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('footer_note', self.gf('django.db.models.fields.TextField')(null=True, blank=True)),
            ('payment_method', self.gf('django.db.models.fields.CharField')(default='bank', max_length=10)),
        ))
        db.send_create_signal(u'invoice', ['Invoice'])

        # Adding model 'InvoiceLine'
        db.create_table(u'invoice_invoiceline', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('invoice', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['invoice.Invoice'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('quantity', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('unit_name', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('unit_price', self.gf('django.db.models.fields.IntegerField')()),
            ('vat_rate', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'invoice', ['InvoiceLine'])

        # Adding model 'InvoicePDF'
        db.create_table(u'invoice_invoicepdf', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('invoice', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['invoice.Invoice'])),
            ('file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('created_at', self.gf('django.db.models.fields.DateTimeField')(auto_now_add=True, blank=True)),
        ))
        db.send_create_signal(u'invoice', ['InvoicePDF'])


    def backwards(self, orm):
        # Deleting model 'Invoice'
        db.delete_table(u'invoice_invoice')

        # Deleting model 'InvoiceLine'
        db.delete_table(u'invoice_invoiceline')

        # Deleting model 'InvoicePDF'
        db.delete_table(u'invoice_invoicepdf')


    models = {
        u'account.account': {
            'Meta': {'object_name': 'Account'},
            'bank_account': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'currency': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'due': ('django.db.models.fields.IntegerField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'iban': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice_email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'registration_no': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'street2': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'swift_bic': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'unit_name': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'vat_mode': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'vat_no': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'vat_price_mode': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'vat_rate': ('django.db.models.fields.IntegerField', [], {}),
            'web': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'zip': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        },
        u'invoice.invoice': {
            'Meta': {'object_name': 'Invoice'},
            'account': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['account.Account']"}),
            'due_on': ('django.db.models.fields.DateField', [], {}),
            'footer_note': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'issued_on': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'note': ('django.db.models.fields.TextField', [], {'null': 'True', 'blank': 'True'}),
            'number': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'payment_method': ('django.db.models.fields.CharField', [], {'default': "'bank'", 'max_length': '10'}),
            'proforma': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'related': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['invoice.Invoice']", 'null': 'True', 'blank': 'True'}),
            'subject': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['subject.Subject']"}),
            'taxable_fulfillment_due': ('django.db.models.fields.DateField', [], {'default': 'datetime.date.today'}),
            'variable_symbol': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'})
        },
        u'invoice.invoiceline': {
            'Meta': {'object_name': 'InvoiceLine'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['invoice.Invoice']"}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'quantity': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'unit_name': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'unit_price': ('django.db.models.fields.IntegerField', [], {}),
            'vat_rate': ('django.db.models.fields.IntegerField', [], {})
        },
        u'invoice.invoicepdf': {
            'Meta': {'object_name': 'InvoicePDF'},
            'created_at': ('django.db.models.fields.DateTimeField', [], {'auto_now_add': 'True', 'blank': 'True'}),
            'file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['invoice.Invoice']"})
        },
        u'subject.subject': {
            'Meta': {'object_name': 'Subject'},
            'bank_account': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'iban': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'registration_no': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'street2': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'swift_bic': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'vat_no': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'web': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'zip': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        }
    }

    complete_apps = ['invoice']