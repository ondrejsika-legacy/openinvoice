# python
import datetime
from cStringIO import StringIO

# django
from django.db import models
from django.core.files import File

# project
from generator import generate


def _list_to_choices(l):
    return map(lambda el: (el, el), l)


class Invoice(models.Model):
    CURRENCIES = ('CZK', )
    PAYMENT_METHODS = ('bank', 'cash', 'cod', 'paypal')

    proforma = models.BooleanField(default=False)
    number = models.CharField(max_length=20, null=True, blank=True)
    variable_symbol = models.IntegerField(null=True, blank=True)
    account = models.ForeignKey('account.Account')
    subject = models.ForeignKey('subject.Subject')
    related = models.ForeignKey('self', null=True, blank=True)
    issued_on = models.DateField(default=datetime.date.today)
    taxable_fulfillment_due = models.DateField(default=datetime.date.today)
    due_on = models.DateField()
    note = models.TextField(null=True, blank=True)
    footer_note = models.TextField(null=True, blank=True)
    payment_method = models.CharField(max_length=10, choices=_list_to_choices(PAYMENT_METHODS), default='bank')
    currency = models.CharField(max_length=3, choices=_list_to_choices(CURRENCIES))

    @property
    def total(self):
        return sum(line.total for line in self.invoiceline_set.all())

    def __unicode__(self):
        return u'%s %s #%s' % (self.account.email, self.number, self.id)

    def generate(self):
        pdf = generate(self)
        InvoicePDF.objects.create(invoice=self, file=File(StringIO(pdf), '%s.pdf' % self.id))


class InvoiceLine(models.Model):
    invoice = models.ForeignKey('invoice.Invoice')
    name = models.CharField(max_length=64)
    quantity = models.IntegerField(null=True, blank=True)
    unit_name = models.CharField(max_length=10, null=True, blank=True)
    unit_price = models.IntegerField()
    vat_rate = models.IntegerField()

    @property
    def total(self):
        return self.unit_price * (self.quantity or 1)

    def __unicode__(self):
        return u'%s %s #%s' % (self.invoice.number, self.name, self.id)


class InvoicePDF(models.Model):
    invoice = models.ForeignKey('invoice.Invoice')
    file = models.FileField(upload_to='invoice/invoicepdf/file')
    created_at = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'%s %s #%s' % (self.invoice.number, self.created_at, self.id)
