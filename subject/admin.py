# django
from django.contrib import admin

# local
from .models import Subject


admin.site.register(Subject)
