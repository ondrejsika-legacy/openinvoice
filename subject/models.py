# django
from django.db import models


class Subject(models.Model):
    name = models.CharField(max_length=64)
    registration_no = models.CharField(max_length=20, null=True, blank=True)
    vat_no = models.CharField(max_length=20, null=True, blank=True)
    street = models.CharField(max_length=64)
    street2 = models.CharField(max_length=64)
    city = models.CharField(max_length=64)
    zip = models.CharField(max_length=10)
    country = models.CharField(max_length=64)
    bank_account = models.CharField(max_length=20)
    iban = models.CharField(max_length=64, null=True, blank=True)
    swift_bic = models.CharField(max_length=32, null=True, blank=True)

    email = models.EmailField()
    phone = models.CharField(max_length=20, null=True, blank=True)
    web = models.URLField(null=True, blank=True)

    def __unicode__(self):
        return u'%s #%s' % (self.email, self.id)

