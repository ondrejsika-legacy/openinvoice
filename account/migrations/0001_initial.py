# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Account'
        db.create_table(u'account_account', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75)),
            ('invoice_email', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('web', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('registration_no', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('vat_no', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('vat_mode', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('vat_price_mode', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('street', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('street2', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('city', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('zip', self.gf('django.db.models.fields.CharField')(max_length=10)),
            ('country', self.gf('django.db.models.fields.CharField')(max_length=64)),
            ('bank_account', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('iban', self.gf('django.db.models.fields.CharField')(max_length=64, null=True, blank=True)),
            ('swift_bic', self.gf('django.db.models.fields.CharField')(max_length=32, null=True, blank=True)),
            ('currency', self.gf('django.db.models.fields.CharField')(max_length=3)),
            ('unit_name', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('vat_rate', self.gf('django.db.models.fields.IntegerField')()),
            ('due', self.gf('django.db.models.fields.IntegerField')()),
        ))
        db.send_create_signal(u'account', ['Account'])


    def backwards(self, orm):
        # Deleting model 'Account'
        db.delete_table(u'account_account')


    models = {
        u'account.account': {
            'Meta': {'object_name': 'Account'},
            'bank_account': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'city': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'country': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'currency': ('django.db.models.fields.CharField', [], {'max_length': '3'}),
            'due': ('django.db.models.fields.IntegerField', [], {}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75'}),
            'iban': ('django.db.models.fields.CharField', [], {'max_length': '64', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'invoice_email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'registration_no': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'street2': ('django.db.models.fields.CharField', [], {'max_length': '64'}),
            'swift_bic': ('django.db.models.fields.CharField', [], {'max_length': '32', 'null': 'True', 'blank': 'True'}),
            'unit_name': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            'vat_mode': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'vat_no': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'vat_price_mode': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            'vat_rate': ('django.db.models.fields.IntegerField', [], {}),
            'web': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'zip': ('django.db.models.fields.CharField', [], {'max_length': '10'})
        }
    }

    complete_apps = ['account']