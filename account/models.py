# django
from django.db import models


def _list_to_choices(l):
    return map(lambda el: (el, el), l)


class Account(models.Model):
    VAT_MODES = ('vat_payer', 'non_vat_payer', 'identified_person')
    VAT_PRICE_MODES = ('without_vat', 'with_vat')
    CURRENCIES = ('CZK', )

    email = models.EmailField()
    invoice_email = models.EmailField(null=True, blank=True)
    phone = models.CharField(max_length=20, null=True, blank=True)
    web = models.URLField(null=True, blank=True)
    name = models.CharField(max_length=64)
    registration_no = models.CharField(max_length=20, null=True, blank=True)
    vat_no = models.CharField(max_length=20, null=True, blank=True)
    vat_mode = models.CharField(max_length=20, choices=_list_to_choices(VAT_MODES))
    vat_price_mode = models.CharField(max_length=20, choices=_list_to_choices(VAT_PRICE_MODES))
    street = models.CharField(max_length=64)
    street2 = models.CharField(max_length=64, null=True, blank=True)
    city = models.CharField(max_length=64)
    zip = models.CharField(max_length=10)
    country = models.CharField(max_length=64)
    bank_account = models.CharField(max_length=20)
    # iban = models.CharField(max_length=64, null=True, blank=True)
    # swift_bic = models.CharField(max_length=32, null=True, blank=True)
    currency = models.CharField(max_length=3, choices=_list_to_choices(CURRENCIES))
    unit_name = models.CharField(max_length=10, null=True, blank=True)
    vat_rate = models.IntegerField()
    due = models.IntegerField()
    note = models.TextField(null=True, blank=True)
    footer_note = models.TextField(null=True, blank=True)

    def __unicode__(self):
        return u'%s #%s' % (self.email, self.id)
